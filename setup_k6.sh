#!/bin/bash
set -ex

apt install -y wget tar
url="https://github.com/grafana/k6/releases/download/v0.50.0/k6-v0.50.0-linux-arm64.tar.gz"
filename="k6-v0.50.0-linux-arm64.tar.gz"

if [ ! -f "$filename" ]; then
    wget "$url"
fi

if [ ! -d "k6-v0.50.0-linux-arm64" ]; then
    tar -xf "$filename"
fi

if [ ! -L "/usr/local/bin/k6" ]; then
    ln -s "$(pwd)/k6-v0.50.0-linux-arm64/k6" /usr/local/bin/k6
fi

